﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
    public partial class Form1 : Form
    {
        private Copo meuCopo;
        public Form1()
        {
            InitializeComponent();
            meuCopo = new Copo();


            BtnCapacidade25.Enabled = false;
            BtnCapacidade35.Enabled = false;
            BtnCapacidade37.Enabled = false;
            BtnCapacidade50.Enabled = false;
            BtnServir.Enabled = false;
            BtnCalculaPercentagem.Enabled = false;
            BtnEncher.Enabled = false;
            BtnEsvaziar.Enabled = false;
        }

        private void BtnCerveja_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = BtnCerveja.Text;
            BtnSumo.Enabled = false;
            BtnVinho.Enabled = false;
            BtnAgua.Enabled = false;

            BtnCapacidade25.Enabled = true;
            BtnCapacidade35.Enabled = true;
            BtnCapacidade37.Enabled = true;
            BtnCapacidade50.Enabled = true;

        }

        private void BtnServir_Click(object sender, EventArgs e)
        {
            LblBebida.Text = meuCopo.Liquido;
            LblCapacidade.Text = meuCopo.Capacidade.ToString();           
            LblContem.Text = meuCopo.Contem.ToString();
            BtnServir.Enabled = false;
            BtnCalculaPercentagem.Enabled = true;
            BtnEncher.Enabled = true;
            BtnEsvaziar.Enabled = true;



            BtnCapacidade25.Enabled = false;
            BtnCapacidade35.Enabled = false;
            BtnCapacidade37.Enabled = false;
            BtnCapacidade50.Enabled = false;


            BtnCerveja.Enabled = false; 
            BtnSumo.Enabled = false;
            BtnVinho.Enabled = false;
            BtnAgua.Enabled = false;

        }

        private void BtnSumo_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = BtnSumo.Text;
            BtnCerveja.Enabled = false;
            BtnVinho.Enabled = false;
            BtnAgua.Enabled = false;

            BtnCapacidade25.Enabled = true;
            BtnCapacidade35.Enabled = true;
            BtnCapacidade37.Enabled = true;
            BtnCapacidade50.Enabled = true;
        }

        private void BtnVinho_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = BtnVinho.Text;
            BtnCerveja.Enabled = false;
            BtnSumo.Enabled = false;
            BtnAgua.Enabled = false;

            BtnCapacidade25.Enabled = true;
            BtnCapacidade35.Enabled = true;
            BtnCapacidade37.Enabled = true;
            BtnCapacidade50.Enabled = true;
        }

        private void BtnAgua_Click(object sender, EventArgs e)
        {
            meuCopo.Liquido = BtnAgua.Text;
            BtnCerveja.Enabled = false;
            BtnSumo.Enabled = false;
            BtnVinho.Enabled = false;

            BtnCapacidade25.Enabled = true;
            BtnCapacidade35.Enabled = true;
            BtnCapacidade37.Enabled = true;
            BtnCapacidade50.Enabled = true;
        }

        private void BtnCapacidade25_Click(object sender, EventArgs e)
        {
            meuCopo.Capacidade = Convert.ToDouble(BtnCapacidade25.Text);
            meuCopo.Contem = meuCopo.Capacidade;
            BtnCapacidade35.Enabled = false;
            BtnCapacidade37.Enabled = false;
            BtnCapacidade50.Enabled = false;
            BtnServir.Enabled = true;
        }

        private void BtnCapacidade35_Click(object sender, EventArgs e)
        {
            meuCopo.Capacidade = Convert.ToDouble(BtnCapacidade35.Text);
            meuCopo.Contem = meuCopo.Capacidade;
            BtnCapacidade25.Enabled = false;
            BtnCapacidade37.Enabled = false;
            BtnCapacidade50.Enabled = false;
            BtnServir.Enabled = true;
        }

        private void BtnCapacidade37_Click(object sender, EventArgs e)
        {
            meuCopo.Capacidade = Convert.ToDouble(BtnCapacidade37.Text);
            meuCopo.Contem = meuCopo.Capacidade;
            BtnCapacidade25.Enabled = false;
            BtnCapacidade35.Enabled = false;
            BtnCapacidade50.Enabled = false;
            BtnServir.Enabled = true;
        }

        private void BtnCapacidade50_Click(object sender, EventArgs e)
        {
            meuCopo.Capacidade = Convert.ToDouble(BtnCapacidade50.Text);
            meuCopo.Contem = meuCopo.Capacidade;
            BtnCapacidade25.Enabled = false;
            BtnCapacidade35.Enabled = false;
            BtnCapacidade37.Enabled = false;
            BtnServir.Enabled = true;
        }

        private void BtnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnCalculaPercentagem_Click(object sender, EventArgs e)
        {


            LblPercentagem.Text =meuCopo.ValorEmPercentagem().ToString() ;

            BtnCalculaPercentagem.Enabled = false;
        }

        private void BtnEncher_Click(object sender, EventArgs e)
        {
            double valor = 2;

            meuCopo.Encher(valor);
            LblBebida.Text = meuCopo.Liquido;
            LblCapacidade.Text = meuCopo.Capacidade.ToString();
            LblContem.Text = meuCopo.Contem.ToString();
            LblPercentagem.Text = meuCopo.ValorEmPercentagem().ToString();
            BtnCalculaPercentagem.Enabled = true;




        }

        private void BtnEsvaziar_Click(object sender, EventArgs e)
        {
            double valor = 2;
            meuCopo.Esvaziar(valor);
            LblBebida.Text = meuCopo.Liquido;
            LblCapacidade.Text = meuCopo.Capacidade.ToString();
            LblContem.Text = meuCopo.Contem.ToString();
            LblPercentagem.Text = meuCopo.ValorEmPercentagem().ToString();
            BtnCalculaPercentagem.Enabled = true;
        }


    }
}
