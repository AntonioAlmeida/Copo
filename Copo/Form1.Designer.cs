﻿namespace Copo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnCerveja = new System.Windows.Forms.Button();
            this.BtnSumo = new System.Windows.Forms.Button();
            this.BtnVinho = new System.Windows.Forms.Button();
            this.Bebidas = new System.Windows.Forms.GroupBox();
            this.BtnAgua = new System.Windows.Forms.Button();
            this.GpQuantidade = new System.Windows.Forms.GroupBox();
            this.BtnCapacidade50 = new System.Windows.Forms.Button();
            this.BtnCapacidade37 = new System.Windows.Forms.Button();
            this.BtnCapacidade35 = new System.Windows.Forms.Button();
            this.BtnCapacidade25 = new System.Windows.Forms.Button();
            this.GpBebidaServida = new System.Windows.Forms.GroupBox();
            this.LblContem = new System.Windows.Forms.Label();
            this.LblCapacidade = new System.Windows.Forms.Label();
            this.LblBebida = new System.Windows.Forms.Label();
            this.LblContemBebida = new System.Windows.Forms.Label();
            this.LblCapacidadeBebida = new System.Windows.Forms.Label();
            this.lblBebidaServida = new System.Windows.Forms.Label();
            this.BtnServir = new System.Windows.Forms.Button();
            this.BtnSair = new System.Windows.Forms.Button();
            this.LblPercentagem = new System.Windows.Forms.Label();
            this.BtnCalculaPercentagem = new System.Windows.Forms.Button();
            this.BtnEncher = new System.Windows.Forms.Button();
            this.BtnEsvaziar = new System.Windows.Forms.Button();
            this.Bebidas.SuspendLayout();
            this.GpQuantidade.SuspendLayout();
            this.GpBebidaServida.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnCerveja
            // 
            this.BtnCerveja.Location = new System.Drawing.Point(29, 46);
            this.BtnCerveja.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCerveja.Name = "BtnCerveja";
            this.BtnCerveja.Size = new System.Drawing.Size(112, 28);
            this.BtnCerveja.TabIndex = 0;
            this.BtnCerveja.Text = "Cerveja";
            this.BtnCerveja.UseVisualStyleBackColor = true;
            this.BtnCerveja.Click += new System.EventHandler(this.BtnCerveja_Click);
            // 
            // BtnSumo
            // 
            this.BtnSumo.Location = new System.Drawing.Point(183, 46);
            this.BtnSumo.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSumo.Name = "BtnSumo";
            this.BtnSumo.Size = new System.Drawing.Size(112, 28);
            this.BtnSumo.TabIndex = 1;
            this.BtnSumo.Text = "Sumo";
            this.BtnSumo.UseVisualStyleBackColor = true;
            this.BtnSumo.Click += new System.EventHandler(this.BtnSumo_Click);
            // 
            // BtnVinho
            // 
            this.BtnVinho.Location = new System.Drawing.Point(367, 46);
            this.BtnVinho.Margin = new System.Windows.Forms.Padding(4);
            this.BtnVinho.Name = "BtnVinho";
            this.BtnVinho.Size = new System.Drawing.Size(112, 28);
            this.BtnVinho.TabIndex = 2;
            this.BtnVinho.Text = "Vinho";
            this.BtnVinho.UseVisualStyleBackColor = true;
            this.BtnVinho.Click += new System.EventHandler(this.BtnVinho_Click);
            // 
            // Bebidas
            // 
            this.Bebidas.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Bebidas.Controls.Add(this.BtnAgua);
            this.Bebidas.Controls.Add(this.BtnCerveja);
            this.Bebidas.Controls.Add(this.BtnVinho);
            this.Bebidas.Controls.Add(this.BtnSumo);
            this.Bebidas.Location = new System.Drawing.Point(133, 38);
            this.Bebidas.Margin = new System.Windows.Forms.Padding(4);
            this.Bebidas.Name = "Bebidas";
            this.Bebidas.Padding = new System.Windows.Forms.Padding(4);
            this.Bebidas.Size = new System.Drawing.Size(691, 138);
            this.Bebidas.TabIndex = 3;
            this.Bebidas.TabStop = false;
            this.Bebidas.Text = "Selecione Bebida";
            // 
            // BtnAgua
            // 
            this.BtnAgua.Location = new System.Drawing.Point(544, 46);
            this.BtnAgua.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgua.Name = "BtnAgua";
            this.BtnAgua.Size = new System.Drawing.Size(112, 28);
            this.BtnAgua.TabIndex = 3;
            this.BtnAgua.Text = "Água";
            this.BtnAgua.UseVisualStyleBackColor = true;
            this.BtnAgua.Click += new System.EventHandler(this.BtnAgua_Click);
            // 
            // GpQuantidade
            // 
            this.GpQuantidade.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GpQuantidade.Controls.Add(this.BtnCapacidade50);
            this.GpQuantidade.Controls.Add(this.BtnCapacidade37);
            this.GpQuantidade.Controls.Add(this.BtnCapacidade35);
            this.GpQuantidade.Controls.Add(this.BtnCapacidade25);
            this.GpQuantidade.Location = new System.Drawing.Point(133, 222);
            this.GpQuantidade.Margin = new System.Windows.Forms.Padding(4);
            this.GpQuantidade.Name = "GpQuantidade";
            this.GpQuantidade.Padding = new System.Windows.Forms.Padding(4);
            this.GpQuantidade.Size = new System.Drawing.Size(691, 112);
            this.GpQuantidade.TabIndex = 4;
            this.GpQuantidade.TabStop = false;
            this.GpQuantidade.Text = "Selecione Quantidade (cl)";
            // 
            // BtnCapacidade50
            // 
            this.BtnCapacidade50.Location = new System.Drawing.Point(544, 37);
            this.BtnCapacidade50.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCapacidade50.Name = "BtnCapacidade50";
            this.BtnCapacidade50.Size = new System.Drawing.Size(112, 28);
            this.BtnCapacidade50.TabIndex = 3;
            this.BtnCapacidade50.Text = "50";
            this.BtnCapacidade50.UseVisualStyleBackColor = true;
            this.BtnCapacidade50.Click += new System.EventHandler(this.BtnCapacidade50_Click);
            // 
            // BtnCapacidade37
            // 
            this.BtnCapacidade37.Location = new System.Drawing.Point(367, 37);
            this.BtnCapacidade37.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCapacidade37.Name = "BtnCapacidade37";
            this.BtnCapacidade37.Size = new System.Drawing.Size(112, 28);
            this.BtnCapacidade37.TabIndex = 2;
            this.BtnCapacidade37.Text = "37,5";
            this.BtnCapacidade37.UseVisualStyleBackColor = true;
            this.BtnCapacidade37.Click += new System.EventHandler(this.BtnCapacidade37_Click);
            // 
            // BtnCapacidade35
            // 
            this.BtnCapacidade35.Location = new System.Drawing.Point(183, 37);
            this.BtnCapacidade35.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCapacidade35.Name = "BtnCapacidade35";
            this.BtnCapacidade35.Size = new System.Drawing.Size(112, 28);
            this.BtnCapacidade35.TabIndex = 1;
            this.BtnCapacidade35.Text = "35";
            this.BtnCapacidade35.UseVisualStyleBackColor = true;
            this.BtnCapacidade35.Click += new System.EventHandler(this.BtnCapacidade35_Click);
            // 
            // BtnCapacidade25
            // 
            this.BtnCapacidade25.Location = new System.Drawing.Point(29, 37);
            this.BtnCapacidade25.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCapacidade25.Name = "BtnCapacidade25";
            this.BtnCapacidade25.Size = new System.Drawing.Size(112, 28);
            this.BtnCapacidade25.TabIndex = 0;
            this.BtnCapacidade25.Text = "25";
            this.BtnCapacidade25.UseVisualStyleBackColor = true;
            this.BtnCapacidade25.Click += new System.EventHandler(this.BtnCapacidade25_Click);
            // 
            // GpBebidaServida
            // 
            this.GpBebidaServida.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.GpBebidaServida.Controls.Add(this.LblContem);
            this.GpBebidaServida.Controls.Add(this.LblCapacidade);
            this.GpBebidaServida.Controls.Add(this.LblBebida);
            this.GpBebidaServida.Controls.Add(this.LblContemBebida);
            this.GpBebidaServida.Controls.Add(this.LblCapacidadeBebida);
            this.GpBebidaServida.Controls.Add(this.lblBebidaServida);
            this.GpBebidaServida.Location = new System.Drawing.Point(133, 341);
            this.GpBebidaServida.Margin = new System.Windows.Forms.Padding(4);
            this.GpBebidaServida.Name = "GpBebidaServida";
            this.GpBebidaServida.Padding = new System.Windows.Forms.Padding(4);
            this.GpBebidaServida.Size = new System.Drawing.Size(234, 158);
            this.GpBebidaServida.TabIndex = 5;
            this.GpBebidaServida.TabStop = false;
            this.GpBebidaServida.Text = "Bebida a Servir";
            // 
            // LblContem
            // 
            this.LblContem.AutoSize = true;
            this.LblContem.Location = new System.Drawing.Point(147, 121);
            this.LblContem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblContem.Name = "LblContem";
            this.LblContem.Size = new System.Drawing.Size(23, 16);
            this.LblContem.TabIndex = 5;
            this.LblContem.Text = "---";
            // 
            // LblCapacidade
            // 
            this.LblCapacidade.AutoSize = true;
            this.LblCapacidade.Location = new System.Drawing.Point(147, 73);
            this.LblCapacidade.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCapacidade.Name = "LblCapacidade";
            this.LblCapacidade.Size = new System.Drawing.Size(23, 16);
            this.LblCapacidade.TabIndex = 4;
            this.LblCapacidade.Text = "---";
            // 
            // LblBebida
            // 
            this.LblBebida.AutoSize = true;
            this.LblBebida.Location = new System.Drawing.Point(142, 20);
            this.LblBebida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblBebida.Name = "LblBebida";
            this.LblBebida.Size = new System.Drawing.Size(23, 16);
            this.LblBebida.TabIndex = 3;
            this.LblBebida.Text = "---";
            // 
            // LblContemBebida
            // 
            this.LblContemBebida.AutoSize = true;
            this.LblContemBebida.Location = new System.Drawing.Point(24, 121);
            this.LblContemBebida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblContemBebida.Name = "LblContemBebida";
            this.LblContemBebida.Size = new System.Drawing.Size(60, 16);
            this.LblContemBebida.TabIndex = 2;
            this.LblContemBebida.Text = "Contem";
            // 
            // LblCapacidadeBebida
            // 
            this.LblCapacidadeBebida.AutoSize = true;
            this.LblCapacidadeBebida.Location = new System.Drawing.Point(24, 73);
            this.LblCapacidadeBebida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCapacidadeBebida.Name = "LblCapacidadeBebida";
            this.LblCapacidadeBebida.Size = new System.Drawing.Size(93, 16);
            this.LblCapacidadeBebida.TabIndex = 1;
            this.LblCapacidadeBebida.Text = "Capacidade";
            // 
            // lblBebidaServida
            // 
            this.lblBebidaServida.AutoSize = true;
            this.lblBebidaServida.Location = new System.Drawing.Point(24, 20);
            this.lblBebidaServida.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBebidaServida.Name = "lblBebidaServida";
            this.lblBebidaServida.Size = new System.Drawing.Size(58, 16);
            this.lblBebidaServida.TabIndex = 0;
            this.lblBebidaServida.Text = "Bebida";
            // 
            // BtnServir
            // 
            this.BtnServir.Location = new System.Drawing.Point(391, 348);
            this.BtnServir.Margin = new System.Windows.Forms.Padding(4);
            this.BtnServir.Name = "BtnServir";
            this.BtnServir.Size = new System.Drawing.Size(112, 28);
            this.BtnServir.TabIndex = 6;
            this.BtnServir.Text = "Servir";
            this.BtnServir.UseVisualStyleBackColor = true;
            this.BtnServir.Click += new System.EventHandler(this.BtnServir_Click);
            // 
            // BtnSair
            // 
            this.BtnSair.BackColor = System.Drawing.Color.Red;
            this.BtnSair.ForeColor = System.Drawing.Color.Yellow;
            this.BtnSair.Location = new System.Drawing.Point(904, 38);
            this.BtnSair.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSair.Name = "BtnSair";
            this.BtnSair.Size = new System.Drawing.Size(112, 138);
            this.BtnSair.TabIndex = 7;
            this.BtnSair.Text = "Sair";
            this.BtnSair.UseVisualStyleBackColor = false;
            this.BtnSair.Click += new System.EventHandler(this.BtnSair_Click);
            // 
            // LblPercentagem
            // 
            this.LblPercentagem.AutoSize = true;
            this.LblPercentagem.Location = new System.Drawing.Point(735, 396);
            this.LblPercentagem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblPercentagem.Name = "LblPercentagem";
            this.LblPercentagem.Size = new System.Drawing.Size(21, 16);
            this.LblPercentagem.TabIndex = 8;
            this.LblPercentagem.Text = "%";
            // 
            // BtnCalculaPercentagem
            // 
            this.BtnCalculaPercentagem.Location = new System.Drawing.Point(682, 348);
            this.BtnCalculaPercentagem.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCalculaPercentagem.Name = "BtnCalculaPercentagem";
            this.BtnCalculaPercentagem.Size = new System.Drawing.Size(142, 28);
            this.BtnCalculaPercentagem.TabIndex = 9;
            this.BtnCalculaPercentagem.Text = "Percentagem";
            this.BtnCalculaPercentagem.UseVisualStyleBackColor = true;
            this.BtnCalculaPercentagem.Click += new System.EventHandler(this.BtnCalculaPercentagem_Click);
            // 
            // BtnEncher
            // 
            this.BtnEncher.Location = new System.Drawing.Point(391, 407);
            this.BtnEncher.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEncher.Name = "BtnEncher";
            this.BtnEncher.Size = new System.Drawing.Size(112, 28);
            this.BtnEncher.TabIndex = 10;
            this.BtnEncher.Text = "Encher";
            this.BtnEncher.UseVisualStyleBackColor = true;
            this.BtnEncher.Click += new System.EventHandler(this.BtnEncher_Click);
            // 
            // BtnEsvaziar
            // 
            this.BtnEsvaziar.Location = new System.Drawing.Point(391, 455);
            this.BtnEsvaziar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEsvaziar.Name = "BtnEsvaziar";
            this.BtnEsvaziar.Size = new System.Drawing.Size(112, 28);
            this.BtnEsvaziar.TabIndex = 11;
            this.BtnEsvaziar.Text = "Esvaziar";
            this.BtnEsvaziar.UseVisualStyleBackColor = true;
            this.BtnEsvaziar.Click += new System.EventHandler(this.BtnEsvaziar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 597);
            this.Controls.Add(this.BtnEsvaziar);
            this.Controls.Add(this.BtnEncher);
            this.Controls.Add(this.BtnCalculaPercentagem);
            this.Controls.Add(this.LblPercentagem);
            this.Controls.Add(this.BtnSair);
            this.Controls.Add(this.BtnServir);
            this.Controls.Add(this.GpBebidaServida);
            this.Controls.Add(this.GpQuantidade);
            this.Controls.Add(this.Bebidas);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Bebidas.ResumeLayout(false);
            this.GpQuantidade.ResumeLayout(false);
            this.GpBebidaServida.ResumeLayout(false);
            this.GpBebidaServida.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnCerveja;
        private System.Windows.Forms.Button BtnSumo;
        private System.Windows.Forms.Button BtnVinho;
        private System.Windows.Forms.GroupBox Bebidas;
        private System.Windows.Forms.Button BtnAgua;
        private System.Windows.Forms.GroupBox GpQuantidade;
        private System.Windows.Forms.Button BtnCapacidade50;
        private System.Windows.Forms.Button BtnCapacidade37;
        private System.Windows.Forms.Button BtnCapacidade35;
        private System.Windows.Forms.Button BtnCapacidade25;
        private System.Windows.Forms.GroupBox GpBebidaServida;
        private System.Windows.Forms.Label LblContem;
        private System.Windows.Forms.Label LblCapacidade;
        private System.Windows.Forms.Label LblBebida;
        private System.Windows.Forms.Label LblContemBebida;
        private System.Windows.Forms.Label LblCapacidadeBebida;
        private System.Windows.Forms.Label lblBebidaServida;
        private System.Windows.Forms.Button BtnServir;
        private System.Windows.Forms.Button BtnSair;
        private System.Windows.Forms.Label LblPercentagem;
        private System.Windows.Forms.Button BtnCalculaPercentagem;
        private System.Windows.Forms.Button BtnEncher;
        private System.Windows.Forms.Button BtnEsvaziar;
    }
}

